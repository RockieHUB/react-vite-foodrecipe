import instance from "./axiosConfig";

const baseUrl = import.meta.env.VITE_API_URL;
const apiGetDaftarResepMakanan = import.meta.env.VITE_API_GETDAFTARRESEPMAKANAN;
const apiGetMyRecipes = import.meta.env.VITE_API_GETMYRECIPES;
const apiGetMyFavoriteRecipes = import.meta.env.VITE_API_GETMYFAVORITERECIPES;
const apiLogin = import.meta.env.VITE_API_SIGNIN;
const apiRegister = import.meta.env.VITE_API_REGISTER;

const buildUrl = (base, params) => {
  let url = base + "?";
  for (const key in params) {
    if (params[key]) {
      url += `${key}=${params[key]}&`;
    }
  }
  // Remove the trailing '&'
  url = url.slice(0, -1);
  return url;
};

//Register
export const userRegister = (formData) => {
  return instance
    .post(apiRegister, formData)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log(error);
      throw error;
    });
};

//Login
export const userLogin = (username, password) => {
  return instance
    .post(apiLogin, { username: username, password: password })
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log(error);
      throw error;
    });
};

//Get Master Category
export const getCategory = () => {
  return instance
    .get("/book-recipe-masters/category-option-lists", {withCredentials:true})
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log(error);
      throw error;
    });
};

//Get Master Level
export const getLevels = () => {
  return instance
    .get("/book-recipe-masters/level-option-lists", {withCredentials:true})
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.error(error);
      throw error;
    });
};

//Post Tambah Resep
export const postTambahResep = (formData) => {
  return instance
    .post("/book-recipe/book-recipes", formData, {withCredentials:true})
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.error(error);
      throw error;
    });
};

//Get Daftar Resep
export const getDaftarResepMakanan = (
  page,
  size,
  recipeName,
  levelId,
  categoryId,
  timeCook,
  sort
) => {
  const apiUrl = buildUrl(apiGetDaftarResepMakanan, {
    page,
    size,
    recipeName,
    levelId,
    categoryId,
    timeCook,
    sort,
  });

  return instance
    .get(apiUrl, {withCredentials:true})
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log("error getting data daftar resep makanan", error);
      throw error;
    });
};

//Get Resep Saya
export const getMyRecipes = (
  page,
  size,
  recipeName,
  levelId,
  categoryId,
  timeCook,
  sort
) => {
  const apiUrl = buildUrl(apiGetMyRecipes, {
    page,
    size,
    recipeName,
    levelId,
    categoryId,
    timeCook,
    sort,
  });

  return instance
    .get(apiUrl, {withCredentials:true})
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log("error getting data daftar resep makanan", error);
      throw error;
    });
};

//Get Daftar Resep Favorit
export const getDaftarResepFavorit = (
  page,
  size,
  recipeName,
  levelId,
  categoryId,
  timeCook,
  sort
) => {
  const apiUrl = buildUrl(apiGetMyFavoriteRecipes, {
    page,
    size,
    recipeName,
    levelId,
    categoryId,
    timeCook,
    sort,
  });

  return instance
    .get(apiUrl, {withCredentials:true})
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log("error getting data daftar resep makanan", error);
      throw error;
    });
};

//Add/Remove to favorite
export const putFavoriteResepMasakan = (recipeId, userId) => {
  const apiUrl = `${baseUrl}/book-recipe/book-recipes/${recipeId}/favorites`;

  return instance
    .put(apiUrl, { userId: userId }, {withCredentials:true})
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log("error edit favorite resep masakan", error);
      throw error;
    });
};

//Delete My Recipe
export const deleteRecipe = async (recipeId, userId) => {
  try {
    const response = await instance.put(
      `${apiGetDaftarResepMakanan}/${recipeId}?userId=${userId}`, {withCredentials:true}
    );

    return {
      success: response.status === 200,
      errorMessage: response.status === 200 ? null : "Failed to delete recipe",
    };
  } catch (error) {
    return {
      success: false,
      errorMessage: `Error deleting recipe: ${error.message}`,
    };
  }
};

//Get Detail Resep
export const getDetailResep = (recipeId) => {
  return instance
    .get(`${apiGetDaftarResepMakanan}/${recipeId}`, {withCredentials:true})
    .then((response) => {
      console.log("response", response);
      return response;
    })
    .catch((error) => {
      console.log(error);
      throw error;
    });
};
